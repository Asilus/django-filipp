from django.db import models
# category
# product
# cart
# order 
# ####
# customer
#specifications

class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя категории')
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name

# class Product(models.Model):
#     title = models.CharField(max_length=255, verbose_name="Наименование")
#     slug = models.SlugField(unique=True)
#     image = models.ImageField()
#     description = models.TextField(verbose_name='Описание', null=True)
#     price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='цена')